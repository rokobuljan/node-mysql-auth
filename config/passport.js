const LocalStrategy = require('passport-local').Strategy;
const db = require('../config/connection');
const bcrypt = require('bcryptjs');

module.exports = (passport) => {

    passport.use(
        'local-login',
        new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // pass request to callback
        }, (req, email, password, done) => {
            if (!email || !password) {
                return done(null, false, req.flash('message', 'All fields are required.'));
            }
            db.query(db.format('SELECT * FROM users WHERE email = ?', [email]), (err, rows) => {
                if (err) {
                    return done(req.flash('message', err));
                }
                console.log("passport.login::%o", rows)
                if (!rows.length) {
                    return done(null, false, req.flash('message', 'Invalid email or password.'));
                }
                if (!bcrypt.compareSync(password, rows[0].password)) {
                    return done(null, false, req.flash('message', 'Invalid email or password.'));
                }
                return done(null, {
                    id: rows[0].id,
                    email: rows[0].email,
                    username: rows[0].username
                });
            });
        })
    );

    passport.use(
        'local-register',
        new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // pass request to callback
        }, (req, email, password, done) => {

            const username = req.body.username;
            const password2 = req.body.password2;

            const queryUserExists = db.format('SELECT * FROM users WHERE email = ?', [email]);
            db.query(queryUserExists, (err, result) => {
                if (err) {
                    return res.status(500).send(err);
                }

                if (result.length > 0) {
                    // User exists error
                    return done(null, false, req.flash('message', 'User already exists'));
                }

                // Check passwords
                if (password !== password2) {
                    return done(null, false, req.flash('message', 'Passwords do not match'));
                }

                // REGISTER
                const hash = bcrypt.hashSync(password, 8);
                // SAVE NEW USER
                const query = db.format('INSERT INTO users (username, email, password) VALUES (?, ?, ?)', [
                    username,
                    email,
                    hash
                ]);

                db.query(query, (err, result) => {
                    if (err) {
                        throw err;
                    }
                    return done(null, {
                        id: result.insertId,
                        email: email,
                        username: username
                    });
                });

            });
        })
    );

    // Serialize to store data in session
    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    // Deserialize session data
    passport.deserializeUser((id, done) => {
        db.query(db.format('SELECT * FROM users WHERE id = ?', [id]), (err, rows) => {
            done(err, {
                id: rows[0].id,
                email: rows[0].email,
                username: rows[0].username
            });
        });
    });

};