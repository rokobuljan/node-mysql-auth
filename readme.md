# NODE.JS + MYSQL AUTH

Mockup for login / register using Node.js and MYSQL

**From scratch:**

___


- Install globally: `npm install express-generator -g`
- Inside a `projects/` folder run `express --view=ejs node-mysql-auth`
- change directory: `$ cd node-mysql-auth` or open the newly created folder in VSCode and open the editor console (`CTRL + ~`)
- Create a `.gitignore` file in root and type `node_modules/` to ignore the node modules folder
- install dependencies: `$ npm install`

in our package.json we should have now:

```json
{
  "name": "node-mysql-auth",
  "version": "0.0.0",
  "private": true,
  "scripts": {
    "start": "node ./bin/www"
  },
  "dependencies": {
    "cookie-parser": "~1.4.3",
    "debug": "~2.6.9",
    "ejs": "~2.5.7",
    "express": "~4.16.0",
    "http-errors": "~1.6.2",
    "morgan": "~1.9.0"
  }
}
```


# MySQL

- Open PHPMyAdmin's console (bottom of window), paste and run the contents of `db/mydb.sql`

# Additional installs

- `$ npm i express-session mysql passport passport-local bcryptjs memorystore connect-flash`

Install Nodemon globally - so we don't have to restart the server after changes  
`npm i nodemon -g`

`$ npm start`  
Run the app: `$ nodemon DEBUG=node-mysql-auth:* npm start`  
head to `http://localhost:3000` you should see the "Welcome to Express" page.

To see Express running:  
`curl -X GET http://localhost:3000 -v`

# Login test account

Login to test account using *email* `test` and *password* `test`

