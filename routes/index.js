const express = require('express');

// Test user logged in - Middleware
const isLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

module.exports = (passport) => {

    const router = express.Router();

    router.get('/', (req, res) => {
        res.render('index', {
            title: "AUTH"
        });
    });

    router.get('/login', (req, res) => {
        res.render('login', {
            title: "AUTH",
            message: req.flash('message')
        });
    });

    router.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
    }));

    router.get('/register', (req, res) => {
        res.render('register', {
            title: "AUTH",
            message: req.flash('message'),
        });
    });

    router.post('/register', passport.authenticate('local-register', {
        successRedirect: '/profile',
        failureRedirect: '/register',
        failureFlash: true
    }));

    router.get('/profile', isLoggedIn, (req, res) => {
        res.render('profile', {
            user: req.user
        });
    });

    router.get('/logout', (req, res) => {
        req.logOut();
        res.redirect('/');
    });

    return router;
};